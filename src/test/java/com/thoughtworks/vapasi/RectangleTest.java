package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

// TODO: Refactor

public class RectangleTest {

    float length = 0.0f;
    float breadth = 0.0f;
    float expectedArea = 0.0f;
    float actualArea = 0.0f;
    float expectedPerimeter = 0.0f;
    float actualPerimeter = 0.0f;


    @Test
    void shouldCaluculateAreaGivenValidInput(){
         length = 10.0f;
         breadth = 5.0f;
         expectedArea = 50.0f;

        Rectangle rectangle = new Rectangle(length, breadth);
        actualArea = rectangle.calculateArea();
        assertEquals(expectedArea, actualArea);

    }

    @Test
    void shouldCaluculatePerimeterGivenValidInput(){
         length = 10.0f;
         breadth = 5.0f;
         expectedPerimeter = 30.0f;

        Rectangle rectangle = new Rectangle(length, breadth);
        actualPerimeter = rectangle.calculatePerimeter();
        assertEquals(expectedPerimeter, actualPerimeter);
    }


}
