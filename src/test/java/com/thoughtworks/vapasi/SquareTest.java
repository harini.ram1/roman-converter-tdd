package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    Square square;

    @BeforeEach
    void setUp() {
        square = new Square(5.0f);
    }

    @Test
    void shouldCalculateAreaOfSquare() {
        float expectedArea = 25.0f;
        assertEquals(expectedArea, square.calculateArea());
    }

    @Test
    void shouldCalculatePerimeterOfSquare() {
        float expectedPerimeter = 20.0f;
        assertEquals(expectedPerimeter, square.calculatePerimeter());
    }
}