package com.thoughtworks.vapasi;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class RomanConverterTest {

    private RomanConverter romanConvertor;
    Integer expect;
    Integer actual;
    String romanNumber;


    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @Test
    void shouldConvertI() {
        //Arrange
         expect = 1;
         romanNumber = "I";

        //Act
        actual = romanConvertor.convertRomanToArabicNumber(romanNumber);

        //Assert
        assertEquals(expect, actual);

    }


    @Test
    @Disabled
    void shouldConvertII() {
        expect = 2;
        romanNumber = "II";
        actual = romanConvertor.convertRomanToArabicNumber(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldConvertIII() {
        expect = 3;
        romanNumber = "III";
        actual = romanConvertor.convertRomanToArabicNumber(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldConvertIV() {

    }

    @Test
    void shouldConvertV() {
    }

    @Test
    void shouldConvertVI() {
    }

    @Test
    void shouldConvertVII() {
    }

    @Test
    void shouldConvertIX() {
    }

    @Test
    void shouldConvertX() {
    }

    @Test
    void shouldConvertXXXVI() {
    }

    @Test
    void shouldConvertMMXII() {
    }

    @Test
    void shouldConvertMCMXCVI() {
    }

    @Test
    @Disabled
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        romanNumber = "wsr";
        assertThrows(IllegalArgumentException.class,
                ()->{
                    romanConvertor.convertRomanToArabicNumber(romanNumber);
                });
    }
}