/*
/*
You could try solving the TDD problem using below approach:
a map for unique values in roman code
get arabic value of each character from map
calculate sum of each converted value and return
 */


package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanConverterUsingMapTest {

    RomanConverterUsingMap romanConverterUsingMap;
    Integer expect = 0;
    Integer actual = 0;
    String romanNumber = "I";


    @BeforeEach
    void setUp() {
        romanConverterUsingMap =new RomanConverterUsingMap();
    }

    @Test
     void shouldReturn1(){
         expect = 1;
         romanNumber = "I";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturn5(){
         expect = 5;
         romanNumber = "V";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturn10(){
        expect = 10;
        romanNumber = "X";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturn50(){
        expect = 50;
        romanNumber = "L";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturn100(){
        expect = 100;
        romanNumber = "C";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        assertEquals(expect, actual);
    }


    @Test
    void shouldReturn2(){
        Integer expect = 2;
        Integer actual;
        String romanNumber = "II";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        System.out.println(actual);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturnZeroGivenInvalidInput(){
        Integer expect = 0;
        Integer actual;
        String romanNumber = "PQRS";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        System.out.println(actual);
        assertEquals(expect, actual);
    }

    @Test
    void shouldReturnZeroGivenInvalidInput2(){
        Integer expect = 0;
        Integer actual;
        String romanNumber = "PVIL";

        actual = romanConverterUsingMap.convertRomanToArabic(romanNumber);
        System.out.println(actual);
        assertEquals(expect, actual);
    }






}
