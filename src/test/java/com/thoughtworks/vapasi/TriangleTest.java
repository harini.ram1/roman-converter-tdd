package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TriangleTest {

    Triangle triangle;
    float side1 = 5.0f;
    float side2 = 5.0f;
    float side3 = 5.0f;

    @BeforeEach
    void setUp() {
        triangle = new Triangle(side1, side2, side3);
    }

    @Test
    void shouldCalculateAreaOfTriangle() {
        assertEquals(10.8253, triangle.calculateArea(), 0.001);

    }

    @Test
    void shouldCalculatePerimeterOfTriangle() {
        assertEquals(15, triangle.calculatePerimeter(), 0.001);

    }
}
