package com.thoughtworks.vapasi;


public class Rectangle implements Shape {

    private float length;
    private float breadth;

    public Rectangle(float length, float breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    @Override
    public float calculateArea() {
        return length * breadth;
    }

    @Override
    public float calculatePerimeter() {
        return (2 * (length + breadth));
    }
}


