package com.thoughtworks.vapasi;

public interface Shape {

    float calculateArea();
    float calculatePerimeter();

}
