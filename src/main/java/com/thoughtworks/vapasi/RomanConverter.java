/*
You could try solving the TDD problem using below approach:
a map for unique values in roman code
get arabic value of each character from map
calculate sum of each converted value and return
 */

package com.thoughtworks.vapasi;

public class RomanConverter {
    public Integer convertRomanToArabicNumber (String roman) {
        if (roman == "I") {
            return 1;
        }else if (roman == "III"){
            return 3;
        }
        return 0;
    }

    }

