package com.thoughtworks.vapasi;

public class Triangle implements Shape {

    private final float side1;
    private final float side2;
    private final float side3;

    public Triangle(float side1, float side2, float side3) {

        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }


    @Override
    public float calculateArea() {
        float semiPerimeter = (side1 + side2 + side3)/2;
        return  (float) Math.sqrt(semiPerimeter * (semiPerimeter - side1)  * (semiPerimeter - side2) * ( semiPerimeter - side3) );

    }

    @Override
    public float calculatePerimeter() {
        return (float) side1 + side2 + side3;
    }
}
