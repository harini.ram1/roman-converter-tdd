package com.thoughtworks.vapasi;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RomanConverterUsingMap {

    public Integer convertRomanToArabic(String romanNumber) {

        HashMap<String, Integer> map = getRomanArabicMap();
        String[] romans = romanNumber.split("");

        int[] numbers = new int[10];

        int result = 0;
        for (int i = 0; i <= romans.length-1 ; i++) {
            if (map.get(romans[i]) == null) return 0;
            numbers[i] = map.get(romans[i]);
        }

        for(int j = 0; j<numbers.length-1;)
        {
            if(numbers[j]<numbers[j+1])
            {
                result += numbers[j+1] -numbers[j];
                j=j+2;

            }
            else
            {
                result += numbers[j];
                j=j+1;
            }
        }

        return result;
    }

    private HashMap<String, Integer> getRomanArabicMap() {
        HashMap<String, Integer> map = new HashMap<String,Integer>();
        map.put("I", 1);
        map.put("V", 5);
        map.put("X", 10);
        map.put("L",50);
        map.put("C",100);
        map.put("D",500);
        map.put("M",1000);
        return map;
    }

}
